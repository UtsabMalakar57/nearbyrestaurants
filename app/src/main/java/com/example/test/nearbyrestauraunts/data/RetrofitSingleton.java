package com.example.test.nearbyrestauraunts.data;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by utsab on 6/18/17.
 */

public class RetrofitSingleton {
    public static RetrofitSingleton RetrofitSingleton = null;
    private static Retrofit retrofit = null;
    private static FourSquareApiService apiService = null;


    private RetrofitSingleton() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .build();


        retrofit = new Retrofit.Builder().
                baseUrl("https://api.foursquare.com/v2/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
        apiService = retrofit.create(FourSquareApiService.class);
    }

    public static FourSquareApiService getApiService() {
        if (RetrofitSingleton == null) {
            RetrofitSingleton = new RetrofitSingleton();
        }
        return apiService;
    }

    public static Retrofit getRetrofit() {
        if (RetrofitSingleton == null) {
            RetrofitSingleton = new RetrofitSingleton();
        }
        return retrofit;
    }


}
