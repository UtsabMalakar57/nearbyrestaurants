package com.example.test.nearbyrestauraunts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebpearls on 12/28/2016.
 */

public class ResponseFourSquare {

    @SerializedName("meta")
    @Expose
    public Meta meta;
    @SerializedName("response")
    @Expose
    public Response response;

    public static class Meta {
        @SerializedName("code")
        @Expose
        public String code;
        @SerializedName("requestId")
        @Expose
        public String requestId;
    }

    public static class Response {
        @SerializedName("headerFullLocation")
        @Expose
        public String headerFullLocation;
        @SerializedName("totalResults")
        @Expose
        public String totalResults;
        @SerializedName("groups")
        @Expose
        public List<Group> listGroup;
    }

    public static class Group {
        @SerializedName("type")
        @Expose
        public String type;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("items")
        @Expose
        public ArrayList<Item> listItems;
    }

    public static class Item {
        @SerializedName("venue")
        @Expose
        public Venue venue;
    }

    public static class Venue {
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("contact")
        @Expose
        public Contact contact;
        @SerializedName("location")
        @Expose
        public Location location;
        @SerializedName("categories")
        @Expose
        public List<Category> categories = null;
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("hours")
        @Expose
        public Hours hours;
        @SerializedName("rating")
        @Expose
        public String rating;
        @SerializedName("photos")
        @Expose
        public Photos photos;
    }

    public static class Contact {

        @SerializedName("phone")
        @Expose
        public String phone;
        @SerializedName("formattedPhone")
        @Expose
        public String formattedPhone;
        @SerializedName("twitter")
        @Expose
        public String twitter;
        @SerializedName("facebook")
        @Expose
        public String facebook;
        @SerializedName("facebookUsername")
        @Expose
        public String facebookUsername;
        @SerializedName("facebookName")
        @Expose
        public String facebookName;

    }

    public static class Location {

        @SerializedName("nearbyAddress")
        @Expose
        public String address;
        @SerializedName("crossStreet")
        @Expose
        public String crossStreet;
        @SerializedName("lat")
        @Expose
        public Float lat;
        @SerializedName("lng")
        @Expose
        public Float lng;
        @SerializedName("postalCode")
        @Expose
        public String postalCode;
        @SerializedName("city")
        @Expose
        public String city;
        @SerializedName("state")
        @Expose
        public String state;
        @SerializedName("country")
        @Expose
        public String country;
        @SerializedName("formattedAddress")
        @Expose
        public String[] formattedAddress;

    }

    public static class Category {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("icon")
        @Expose
        public Icon icon;

    }

    public static class Icon {

        @SerializedName("prefix")
        @Expose
        public String prefix;
        @SerializedName("suffix")
        @Expose
        public String suffix;

    }

    public static class Hours {

        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("isOpen")
        @Expose
        public Boolean isOpen;
        @SerializedName("isLocalHoliday")
        @Expose
        public Boolean isLocalHoliday;

    }

    public static class Photos {
        @SerializedName("count")
        @Expose
        public Integer count;
        @SerializedName("groups")
        @Expose
        public List<PhotoGroups> photoGroups = null;
    }

    public static class PhotoGroups {
        @SerializedName("type")
        @Expose
        public String type;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("count")
        @Expose
        public Integer count;
        @SerializedName("items")
        @Expose
        public List<PhotoItems> photoItems = null;
    }

    public static class PhotoItems {
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("prefix")
        @Expose
        public String prefix;
        @SerializedName("suffix")
        @Expose
        public String suffix;
        @SerializedName("width")
        @Expose
        public Integer width;
        @SerializedName("height")
        @Expose
        public Integer height;
    }
}
