package com.example.test.nearbyrestauraunts.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.test.nearbyrestauraunts.R;
import com.example.test.nearbyrestauraunts.model.ResponseFourSquare;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by utsab on 9/11/17.
 */

public class PlacesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private Context context;
    private ArrayList<ResponseFourSquare.Item> venue;
    private VenueClickedCallBack callBack;
    private ArrayList<ResponseFourSquare.Item> filterdResults;
    private VenueFiler friendFilter;

    public PlacesAdapter(Context context, ArrayList<ResponseFourSquare.Item> venue, VenueClickedCallBack callBack) {
        this.context = context;
        this.venue = venue;
        this.callBack = callBack;
        this.filterdResults = venue;

        getFilter();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.layout_list, parent, false);
        RecycleViewHolder viewholder = new RecycleViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        RecycleViewHolder viewHolder = (RecycleViewHolder) holder;
        if (filterdResults.get(position).venue.name != null && !filterdResults.get(position).venue.name.equalsIgnoreCase("")){
            viewHolder.tvName.setText(filterdResults.get(position).venue.name.toString());
        }
        if (filterdResults.get(position).venue.location.formattedAddress != null && !filterdResults.get(position).venue.location.formattedAddress[0].equalsIgnoreCase("")){
            viewHolder.tvAddress.setText(filterdResults.get(position).venue.location.formattedAddress[0]);
        }
        if (filterdResults.get(position).venue.photos.photoGroups.get(0).photoItems.get(0).prefix != null
                && !filterdResults.get(position).venue.photos.photoGroups.get(0).photoItems.get(0).prefix.equalsIgnoreCase("")){
            Picasso.with(this.context)
                    .load(filterdResults.get(position).venue.photos.photoGroups.get(0).photoItems.get(0).prefix+200+
                            filterdResults.get(position).venue.photos.photoGroups.get(0).photoItems.get(0).suffix)
                    .resize(200,200)
                    .into(viewHolder.ivPicture);
        }

        viewHolder.llHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onVenueClicked(filterdResults.get(position).venue);
            }
        });

    }

    @Override
    public int getItemCount() {
        return filterdResults.size();
    }

    @Override
    public Filter getFilter() {
        if (friendFilter == null) {
            friendFilter = new VenueFiler();
        }

        return friendFilter;
    }

    public class RecycleViewHolder extends RecyclerView.ViewHolder{

        private View view;
        TextView tvName;
        TextView tvAddress;
        ImageView ivPicture;
        LinearLayout llHolder;
        public RecycleViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvAddress = (TextView) view.findViewById(R.id.tv_address);
            ivPicture = (ImageView) view.findViewById(R.id.iv_picture);
            llHolder = (LinearLayout) view.findViewById(R.id.ll_venue_holder);
        }
    }

    private class VenueFiler extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<ResponseFourSquare.Item> tempList = new ArrayList<ResponseFourSquare.Item>();

                // search content in friend list
                for (ResponseFourSquare.Item user : venue) {
                    if (user.venue.name.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(user);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = venue.size();
                filterResults.values = venue;
            }

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            filterdResults = (ArrayList<ResponseFourSquare.Item>) filterResults.values;
            notifyDataSetChanged();
        }
    }

        public interface VenueClickedCallBack{
        void onVenueClicked(ResponseFourSquare.Venue venue);
    }
}
