package com.example.test.nearbyrestauraunts.data;

import com.example.test.nearbyrestauraunts.model.CustomLatLongModel;
import com.example.test.nearbyrestauraunts.model.ResponseFourSquare;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by ebpearls on 12/30/2016.
 */

public interface FourSquareApiService {
    /*
    * https://api.foursquare.com/v2/venues/explore
    * ?client_id=YX51331PKXHCSCU4SIZE5YNAV5TXBZ3MDCH0GJAKFLPWZNY1
    * &client_secret=T1G3ARJW0PEKQBCVKNC2DPLRD2W5GPPV2VQ1U1V4FNB4H1QN
    * &v=20131124
    * &ll=40.7463956,-73.9852992*/
    @GET("venues/explore")
    Call<ResponseFourSquare> getFourSquarePlaces(@QueryMap() Map<String, String> options);

    @GET("venues/suggestcompletion")
    Call<ResponseFourSquare> getFourSquarePlacesSearch(@QueryMap() Map<String, String> options);

    @GET("geocode/json")
    Call<CustomLatLongModel> getCurrentLocation(@Query("address") String address);
}
