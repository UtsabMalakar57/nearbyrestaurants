package com.example.test.nearbyrestauraunts.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ebpearls on 1/11/2017.
 */

public class RequestFourSquare {
    @SerializedName("venue")
    public String venue;

    @SerializedName("nearbyAddress")
    public String nearbyAddress;

    public String latitude;

    public String longitude;

    @Override
    public String toString() {
        return "RequestFourSquare{" +
                "venue='" + venue + '\'' +
                ", nearbyAddress='" + nearbyAddress + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}
