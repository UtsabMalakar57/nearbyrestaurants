package com.example.test.nearbyrestauraunts;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.test.nearbyrestauraunts.adapter.PlacesAdapter;
import com.example.test.nearbyrestauraunts.data.FourSquareApiService;
import com.example.test.nearbyrestauraunts.data.RetrofitSingleton;
import com.example.test.nearbyrestauraunts.model.GPSTracker;
import com.example.test.nearbyrestauraunts.model.RequestFourSquare;
import com.example.test.nearbyrestauraunts.model.ResponseFourSquare;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListActivity extends AppCompatActivity implements PlacesAdapter.VenueClickedCallBack, SearchView.OnQueryTextListener {

    private String myLatLong;
    private ArrayList<ResponseFourSquare.Item> venue;
    private PlacesAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    private RecyclerView rvList;
    private MenuItem searchMenuItem;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        rvList = (RecyclerView) findViewById(R.id.rv_list);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        setActionBar();
        checkPermissionLocation();


    }

    private void setActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("Foodland");

    }

    private void getDataFromFourSquare() {
        FourSquareApiService fourSquareApiService = RetrofitSingleton.getApiService();
        RequestFourSquare requestFourSquare = new RequestFourSquare();
        Map<String, String> map = new HashMap<>();
        map.put("client_id", "YX51331PKXHCSCU4SIZE5YNAV5TXBZ3MDCH0GJAKFLPWZNY1");
        map.put("client_secret", "T1G3ARJW0PEKQBCVKNC2DPLRD2W5GPPV2VQ1U1V4FNB4H1QN");
        map.put("v", "20131124");
        map.put("section", "food");
        map.put("venuePhotos", "1");
        map.put("radius","500");
        map.put("limit", "20");

        map.put("query", requestFourSquare.nearbyAddress == null ? "" : requestFourSquare.nearbyAddress);

        //If the near string is not geocodable, returns a failed_geocode error.
        // Otherwise, searches within the bounds of the geocode and adds a geocode object to the response.
        //Required: latlng or nearby address,any one must be set
        String latLng = myLatLong;
        String nearbyAddress = "";
        if (latLng != null && !latLng.isEmpty() && !latLng.equalsIgnoreCase("0.0,0.0")) {
            //Set lat lng
            map.put("ll", latLng);
            nearbyAddress = requestFourSquare.nearbyAddress == null ? "" : requestFourSquare.nearbyAddress;
        } else {
            //set nearby address
            if (requestFourSquare.nearbyAddress == null || requestFourSquare.nearbyAddress.equalsIgnoreCase("")) {
                nearbyAddress = "Kathmandu";
            } else {
                nearbyAddress = requestFourSquare.nearbyAddress;
            }
        }
        map.put("near", nearbyAddress);
        Call<ResponseFourSquare> call = fourSquareApiService.getFourSquarePlaces(map);
        call.enqueue(new Callback<ResponseFourSquare>() {
            @Override
            public void onResponse(Call<ResponseFourSquare> call, Response<ResponseFourSquare> response) {
                if (response.isSuccessful()){
//                    Toast.makeText(ListActivity.this,response.body().response.listGroup.get(0).name,Toast.LENGTH_SHORT).show();
                    populateSpinner(response.body().response.listGroup.get(0).listItems);
                }
            }

            @Override
            public void onFailure(Call<ResponseFourSquare> call, Throwable t) {

            }
        });

    }

    private void populateSpinner(ArrayList<ResponseFourSquare.Item> venue) {
//        this.venue = venue;
//        ArrayList<String> list = new ArrayList<>();
//        for (int i = 0; i < venue.size(); i++){
//            list.add(i,venue.get(i).venue.name);
//        }
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,list);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(adapter);
        if (adapter == null){
            adapter = new PlacesAdapter(this, venue, this);
            rvList.setLayoutManager(linearLayoutManager);
            rvList.setAdapter(adapter);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_venue, menu);

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.search);
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);

        return true;
    }

    public static void openListActivity(Context context) {
        Intent intent = new Intent(context, ListActivity.class);
        context.startActivity(intent);
    }

    private void checkPermissionLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION}, 1005);
            return;
        } else {
            setMyLocation();
        }
    }

    private void setMyLocation() {
        GPSTracker gps;
        gps = new GPSTracker(this);
        double lat, lng;
        String latlng = null;


        // Check if GPS enabled
        if (gps.canGetLocation()) {
            lat = gps.getLatitude();
            lng = gps.getLongitude();
            latlng = String.valueOf(lat) + "," + String.valueOf(lng);
            myLatLong = latlng;
            if (myLatLong != null && !myLatLong.equalsIgnoreCase("") && !myLatLong.equalsIgnoreCase("0.0,0.0")) {
                getDataFromFourSquare();
            }else{
                Toast.makeText(this, "Error! Unable to get your current location", Toast.LENGTH_SHORT).show();
            }
//            Toast.makeText(ListActivity.this, myLatLong, Toast.LENGTH_SHORT).show();

        } else {
            // Can't get location.
            // GPS or network is not enabled.
            // Ask user to enable GPS/network in settings.
            Toast.makeText(ListActivity.this, "Failed getting location", Toast.LENGTH_SHORT).show();
            gps.showSettingsAlert();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1005:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    setMyLocation();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        StaticModelClass.isLoggedIn = false;
        super.onDestroy();
    }

    @Override
    public void onVenueClicked(ResponseFourSquare.Venue venue) {
        Intent intent = new Intent(this, DetailActivity.class);
        String json = new Gson().toJson(venue);
        intent.putExtra("VenueModel", json);
        startActivity(intent);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        adapter.getFilter().filter(s);
        return true;
    }
}
