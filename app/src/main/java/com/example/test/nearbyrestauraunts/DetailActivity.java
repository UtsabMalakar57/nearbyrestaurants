package com.example.test.nearbyrestauraunts;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.test.nearbyrestauraunts.model.ResponseFourSquare;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;

public class DetailActivity extends AppCompatActivity implements OnMapReadyCallback {

    private ResponseFourSquare.Venue venueModel;

    ImageView ivBack;
    ImageView ivStar1;
    ImageView ivStar2;
    ImageView ivStar3;
    ImageView ivStar4;
    ImageView ivStar5;
    ImageView ivBanner;
    ImageView fakemap;

    ScrollView scrollView;

    TextView tvToolbarTitle;
    TextView tvOpenStatus;
    TextView tvContact;
    TextView tvUrl;
    TextView tvPicTitle;
    MapView mMapView;


    private GoogleMap googleMap;
    private String latitude;
    private String longitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        String model = getIntent().getExtras().getString("VenueModel");
        Type type = new TypeToken<ResponseFourSquare.Venue>() {
        }.getType();
        venueModel = new Gson().fromJson(model, type);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        ivStar1 = (ImageView) findViewById(R.id.iv_star1);
        ivStar2 = (ImageView) findViewById(R.id.iv_star2);
        ivStar3 = (ImageView) findViewById(R.id.iv_star3);
        ivStar4 = (ImageView) findViewById(R.id.iv_star4);
        ivStar5 = (ImageView) findViewById(R.id.iv_star5);
        fakemap = (ImageView) findViewById(R.id.map_fake);
        scrollView = (ScrollView) findViewById(R.id.scollview);

        tvOpenStatus = (TextView) findViewById(R.id.tv_open_status);
        tvContact = (TextView) findViewById(R.id.tv_contact_status);
        tvUrl = (TextView) findViewById(R.id.tv_url);


        ivBanner = (ImageView) findViewById(R.id.iv_venue_pic);
        tvPicTitle = (TextView) findViewById(R.id.tv_title);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        mMapView = (MapView) findViewById(R.id.google_map);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
            fakemap.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    scrollView.requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });

        }
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        setupView();
    }


    private void setupView() {
        if (venueModel.rating != null && !venueModel.rating.equalsIgnoreCase("")) {
            float rating = Float.parseFloat(venueModel.rating);
            if (rating >= 1 && rating < 2) {
                ivStar1.setVisibility(View.VISIBLE);
            } else if (rating >= 2 && rating < 4) {
                ivStar1.setVisibility(View.VISIBLE);
                ivStar2.setVisibility(View.VISIBLE);
            } else if (rating >= 4 && rating < 6) {
                ivStar1.setVisibility(View.VISIBLE);
                ivStar2.setVisibility(View.VISIBLE);
                ivStar3.setVisibility(View.VISIBLE);
            } else if (rating >= 6 && rating < 8) {
                ivStar1.setVisibility(View.VISIBLE);
                ivStar2.setVisibility(View.VISIBLE);
                ivStar3.setVisibility(View.VISIBLE);
                ivStar4.setVisibility(View.VISIBLE);
            }else if (rating >= 6 && rating < 8){
                ivStar1.setVisibility(View.VISIBLE);
                ivStar2.setVisibility(View.VISIBLE);
                ivStar3.setVisibility(View.VISIBLE);
                ivStar4.setVisibility(View.VISIBLE);
                ivStar5.setVisibility(View.VISIBLE);
            }else{
                ivStar1.setVisibility(View.VISIBLE);
                ivStar2.setVisibility(View.INVISIBLE);
                ivStar3.setVisibility(View.INVISIBLE);
                ivStar4.setVisibility(View.INVISIBLE);
                ivStar5.setVisibility(View.INVISIBLE);
            }
        }else{
            ivStar1.setVisibility(View.VISIBLE);

        }
        if (!TextUtils.isEmpty(venueModel.name)) {
            tvToolbarTitle.setText(venueModel.name);
            tvPicTitle.setText(venueModel.name);
        }

        if (venueModel.location.lat != null){
            this.latitude = String.valueOf(venueModel.location.lat);
            this.longitude = String.valueOf(venueModel.location.lng);
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        float density = getResources().getDisplayMetrics().density;
        int height = Math.round(displayMetrics.heightPixels / density);
        int width = Math.round(displayMetrics.widthPixels / density);
        if (!TextUtils.isEmpty(venueModel.photos.photoGroups.get(0).photoItems.get(0).prefix)){
            Picasso.with(this)
                    .load(venueModel.photos.photoGroups.get(0).photoItems.get(0).prefix+300+
                            venueModel.photos.photoGroups.get(0).photoItems.get(0).suffix)
                    .resize(width,300)
                    .into(ivBanner);
        }
        if (googleMap != null)
            setLocation(googleMap, latitude, longitude);

        if (venueModel.contact != null && venueModel.contact.formattedPhone != null){
            tvContact.setText("Contact: "+ venueModel.contact.formattedPhone);
        }else{
            tvContact.setText("Contact: Not Available");
        }
        if (venueModel.hours != null && venueModel.hours.isOpen != null){
            if (venueModel.hours.isOpen){
                tvOpenStatus.setText("Opening Time: Open Now");
                tvOpenStatus.setTextColor(getResources().getColor(R.color.green));
            }else{
                tvOpenStatus.setText("Opening Time: Closed");
                tvOpenStatus.setTextColor(getResources().getColor(R.color.red));
            }
        }else{
            tvOpenStatus.setText("Opening Time: Not Available");
        }

        if (venueModel.url != null){
            tvUrl.setText("Website: "+ venueModel.url);
        }else{
            tvUrl.setText("Website: Not Available");
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(this);
        this.googleMap = googleMap;
        if (latitude != null && !latitude.equalsIgnoreCase(""))
            setLocation(googleMap, latitude, longitude);
    }

    private void setLocation(GoogleMap googleMap, String latitude, String longitude) {
        LatLng latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        View locationButton = ((View) mMapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
// position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 0, 30);
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.googleMap.getUiSettings().setMapToolbarEnabled(true);
        this.googleMap.addMarker(new MarkerOptions()
                .position(latLng));
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
    }
}
