package com.example.test.nearbyrestauraunts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (StaticModelClass.isLoggedIn)
            ListActivity.openListActivity(this);
        else
            MainActivity.openMainActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
