package com.example.test.nearbyrestauraunts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Utsab on 2/1/2017.
 */

public class CustomLatLongModel {
    @SerializedName("results")
    @Expose
    public List<Result> results = null;
    @SerializedName("status")
    @Expose
    public String status;


    public static class Result {

        @SerializedName("formatted_address")
        @Expose
        public String formattedAddress;
        @SerializedName("geometry")
        @Expose
        public Geometry geometry;
        @SerializedName("place_id")
        @Expose
        public String placeId;
        @SerializedName("types")
        @Expose
        public List<String> types = null;


        public class Geometry {
            @SerializedName("location")
            @Expose
            public Location location;

            public class Location {
                @SerializedName("lat")
                @Expose
                public Double lat;
                @SerializedName("lng")
                @Expose
                public Double lng;
            }
        }

    }

}
